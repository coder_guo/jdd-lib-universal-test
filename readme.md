# 分支

目前开发分支是 vite

# 开发

<del> run `npm run dev:lib` 启动库开发打包 </del> 通过predev处理了，不需要再手动运行了

run `npm run dev` 启动测试页面

然后修改 example 中的内容即可

# 打包

代码提交之前需要执行打包操作``` npm run build ```，目前是直接安装的git仓库，所以需要手动打包

# git 仓库设置，同时推送两个仓库

工蜂: git@git.code.tencent.com:lib-universal/jdd-lib-universal.git
gitlab: git@gitlab.com:douxc512/jdd-lib-universal.git

git remote get-url origin
如果已经有值了，直接再添加一个源`git remote set-url --add origin xxx`

import { ErrorMessageMap } from './Error';

// 封装统一的请求
const headers = {
  'content-type': 'application/json; charset=utf-8;',
};
/** 从服务器返回的数据结构 */
interface ServerResponse<T> {
  code: number;
  data: T;
  message: string | null;
  /** 接口返回错误的时候有status没有code */
  status?: number;
  /** sso 平台返回会有result */
  result?: number;
}
// 默认自动登录处理
let reLoginFn = () => {
  // 没有token
  setTimeout(() => {
    // 重新登录
    window.location.replace(`/login?redirect=${encodeURIComponent(window.location.href)}`);
  }, 1000);
};
export function setReLoginHandle(fn: () => void) {
  reLoginFn = fn;
}

/** 顶部toast展示错误信息 */
async function message(msg: string) {
  const messageEle = document.createElement('div');
  messageEle.id = 'jdd-fetch-error-message';
  messageEle.setAttribute(
    'style',
    'max-width:640px;padding:15px;color:red;border:1px solid #ccc;position:fixed;top:10px;z-index:9999',
  );
  messageEle.innerText = msg;
  document.body.append(messageEle);
  setTimeout(() => {
    messageEle.remove();
  }, 1500);
}

/** 请求成功之后根据业务code处理 */
function parseResponse<T>(response: ServerResponse<T>) {
  if (response.code === 200) {
    return Promise.resolve(response.data);
  } else if (response.code === 82024 || response.code === 82023 || response.result === 401) {
    // sso 认证平台登录过期
    message(response.message || ErrorMessageMap[response.code]);
    reLoginFn();
    return;
  } else {
    // 默认提示错误信息
    message(response.message || `业务接口错误,Status:${response.status}`);
    return Promise.reject(response);
  }
}
// eslint-disable-next-line no-undef
interface FetcherInitialOptions extends RequestInit {
  params?: Record<string, any>;
  onResponse?: (v: any) => void;
}
/** 默认的请求函数，对原生的fetch添加了处理 */
const fetcher = <T = null>(url: string, init: FetcherInitialOptions = { method: 'GET' }) => {
  // 默认取配置的headers，如果init传入，则以传入的为准
  const targetHeaders = new Headers({ ...init.headers });
  // add token to headers
  if (localStorage.getItem('token')) {
    targetHeaders.set('authorization', `Bearer ${localStorage.getItem('token')}`);
  }
  // 如果是post请求，自动加上header
  let fetchUrl = url;
  if (init.method === 'POST' && typeof init.body === 'string') {
    targetHeaders.append('content-type', headers['content-type']);
  } else if (!init?.method || init.method === 'GET') {
    // get 请求，将params自动转为urlParams
    const urlParams = new URLSearchParams();
    if (init.params) {
      Object.keys(init.params).forEach((key) => {
        const value = init.params![key];
        if (value !== null && value !== undefined) {
          urlParams.append(key, value);
        }
      });
    }
    if (urlParams.toString() !== '') {
      fetchUrl = url.indexOf('?') !== -1 ? `${url}&${urlParams.toString()}` : `${url}?${urlParams.toString()}`;
    }
    delete init.params;
  }
  return fetch(fetchUrl, {
    ...init,
    headers: targetHeaders,
    credentials: 'include',
  })
    .then((r) => {
      // 请求成功，针对网络请求的状态码进行处理
      const { status } = r;
      if (status === 401) {
        message('登录已过期，请重新登录');
        reLoginFn();
        return Promise.reject(new Error('登录过期'));
      }
      if (status === 404) {
        message(`接口返回404,url: ${url}`);
        return Promise.reject(new Error(`接口返回404,url: ${url}`));
      }
      return r.clone();
    })
    .then(async (ret: Response) => {
      if (ret.headers.get('content-disposition')) {
        // attachment;filename=%E5%AF%B9%E8%B4%A6%E5%8D%95.xlsx;filename*=utf-8''%E5%AF%B9%E8%B4%A6%E5%8D%95.xlsx
        const fileBlob = await ret.blob();
        const a = document.createElement('a');
        const link = window.URL.createObjectURL(fileBlob);
        const filenameStr = ret.headers.get('content-disposition')?.split(';')[1] as string;
        const filename = decodeURIComponent(filenameStr).replace(new RegExp('filename=', 'g'), '');
        if (filename.endsWith('html')) {
          // 打印单据时的html
          return link as any as T;
        }
        a.href = link;
        a.download = filename || 'file.xlsx';
        a.click();
        return;
      }
      init.onResponse?.(ret);
      // 解析成功，针对自定义code处理
      try {
        return parseResponse((await ret.json()) as ServerResponse<T>);
      } catch (error) {
        console.error(error);
      }
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};

export default fetcher;

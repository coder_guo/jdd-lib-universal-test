//@ts-ignore
import FileUpload from './upload/Upload.vue';
import { FileStatus, FileAcceptType } from './upload/core';

export { FileUpload, FileAcceptType, FileStatus };

// 文件上传，附件模式
/**
 * 渲染页面，共有两种
 * ```
 * 1. 单图片模式，选择后即预览
 * 2. 附件模式，选择后依次展示，点击后预览
 *```
 */
import './index.css';
import FileUpload, {
  FileAcceptType,
  FileStatus,
  WrappedFile,
  Config,
  generateUUID,
  getFileNameFromUrl,
  getFileSuffixFromUrl,
} from './core';

/** 单图片模式配置项 */
interface AttachmentUIConfig extends Partial<Omit<Config, 'onNotify'>> {
  rootElement: HTMLElement;
  onNotify?: (files: WrappedFile[] | null) => void;
}

/** 通用文件上传 - 附件模式 */
export class Attachment {
  // 上传实例
  private uploadInstance: FileUpload;
  // 触发文件选择的元素
  private selectElement: HTMLElement;
  private filesElement: HTMLElement;
  // 所有的文件
  private allFiles: Map<string, WrappedFile> = new Map();
  // maxCount default 5
  private maxCount: number = 5;
  // 文件变化通知函数，如果是删除了文件，则传入null
  private _onNotify: AttachmentUIConfig['onNotify'];
  // 只读模式
  private readonly: boolean = false;
  // id前缀
  private _idPrefix: string;
  // File Accept
  private accept: FileAcceptType;
  constructor(options: AttachmentUIConfig) {
    const _this = this;
    this._idPrefix = generateUUID();
    this._onNotify = options.onNotify;
    this.readonly = options.readonly ?? false;
    this.maxCount = options.maxCount || 5;
    this.accept = options.accept || FileAcceptType.ALL;
    this.uploadInstance = new FileUpload({
      readonly: options.readonly,
      maxCount: options.maxCount || 5,
      accept: options.accept || FileAcceptType.ALL,
      OSS_PREFIX: options.OSS_PREFIX || 'https://statics.wellyspring.com',
      getFileUploadURL: options.getFileUploadURL,
      onNotify(file) {
        _this.allFiles.set(file.uuid, file);
        // 触发所有文件渲染
        _this.renderAllFiles();
      },
    });
    this.render(options.rootElement);
  }
  /** 根据文件状态生成文件item */
  renderFileItem(file: WrappedFile) {
    return `<div class="jdd-file-upload_attachment--item" style="border:${
      file.status === FileStatus.FAILED ? '1px dashed red' : ''
    }">
    ${
      file.status !== FileStatus.FAILED
        ? ` <div data-uuid='${
            file.uuid
          }' data-action='preview' class='jdd-file-upload_attachment--item-type'>${getFileSuffixFromUrl(
            file.thumbURL,
          )}</div>`
        : ''
    }
       
        <div style="flex: 1;width:calc(100% - 45px);padding-left:5px;position: relative;">
          <div class="jdd-file-upload_attachment--item-name" name="${file.name}">${file.name}${
      file.status === FileStatus.FAILED
        ? '&nbsp;<span class="icon-danger text-danger"style="position: absolute;top:2px"></span>'
        : ''
    }</div>
          
          <div class="jdd-file-upload_attachment--item-icons">
          ${file.status === FileStatus.UPLOADING ? '<span class="icon-shape-half-circle anim-loading"></span>' : ''}
          
          ${file.status === FileStatus.SUCCESS ? '<span class="icon-check-o text-success"></span>' : ''}
          ${
            file.status !== FileStatus.FAILED && !this.readonly
              ? ` <span data-action="delete" data-uuid="${file.uuid}" class="icon-trash text-danger"></span>`
              : ''
          }
          ${
            file.status === FileStatus.FAILED
              ? `<div> <span style="color:#2491FC">重新上传</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span data-action="delete" data-uuid="${file.uuid}"  style="color:#2491FC">删除</span></div>`
              : ''
          }
         
          </div>
        </div>
    </div>`;
  }
  renderAllFiles() {
    const files: string[] = [];
    // 如果此时所有文件超过了maxCount，应该隐藏文件选择
    if (this.allFiles.size >= this.maxCount) {
      this.selectElement.classList.add('hide');
    } else {
      this.selectElement.classList.remove('hide');
    }
    this.allFiles.forEach((file) => {
      files.push(this.renderFileItem(file));
    });
    this.filesElement.innerHTML = files.join('');
    this._onNotify?.(Array.from(this.allFiles.values()));
  }
  /** 使用特定的值渲染，用于默认值处理 */
  renderDefaultFile(defaultValue: string[]) {
    const _this = this;
    if (defaultValue && defaultValue.length > 0) {
      defaultValue.forEach((value) => {
        const defaultFile: WrappedFile = {
          thumbURL: value,
          name: getFileNameFromUrl(value),
          status: FileStatus.SUCCESS,
          originFileObj: null,
          uuid: generateUUID(),
        };
        _this.allFiles.set(defaultFile.uuid, defaultFile);
      });
      _this.renderAllFiles();
    }
  }

  /** 渲染html结构 */
  render(rootElement: HTMLElement) {
    const _this = this;
    // 是否应该隐藏文件选择，只读
    const shouldHideSelect = _this.readonly;
    const innerHTML = `
     <div id="${this._idPrefix}-container_attachment" class="jdd-file-upload-container_attachment">
      <div id="${this._idPrefix}-select" class="jdd-file-upload-select_attachment ${shouldHideSelect ? 'hide' : ''}">
        <p>+</p>
        <p>点击选择文件</p>
        <p class="jdd-file-upload-select_tip">${this.formatFileAccept()}</p>
      </div>
       <div id="${this._idPrefix}-list" class="jdd-file-list_attachment"></div>
     </div>
   `;
    rootElement.innerHTML = innerHTML;
    this.selectElement = document.getElementById(`${this._idPrefix}-select`);
    this.filesElement = document.getElementById(`${this._idPrefix}-list`);
    this.selectElement.addEventListener('click', () => {
      this.uploadInstance.choose();
    });
    document.getElementById(`${this._idPrefix}-list`).addEventListener('click', function (e: any) {
      e.preventDefault();
      e.stopPropagation();
      const { uuid, action } = e.target.dataset || {};
      if (action && action.length > 0 && uuid) {
        const file = _this.allFiles.get(uuid);
        if (action === 'preview') {
          // 预览
          window.open(file.thumbURL);
        } else if (action === 'delete') {
          // 删除
          _this.allFiles.delete(uuid);
          _this.renderAllFiles();
        }
      } else {
        // not target dom, do nothing
      }
    });
  }
  /** 将Accept的文件类型默认展示，方便用户选择时判断
   *
   * accept=.pdf,image/*,*
   */
  formatFileAccept() {
    const result = this.accept.split(',').map((acceptStr) => {
      if (acceptStr === FileAcceptType.AUDIO) {
        return '音频';
      } else if (acceptStr === FileAcceptType.IMAGE) {
        return '图片';
      } else if (acceptStr === FileAcceptType.VIDEO) {
        return '视频';
      } else if (acceptStr === FileAcceptType.PDF) {
        return 'PDF';
      } else if (acceptStr === FileAcceptType.OFFICE) {
        return 'Word/Excel/PPT';
      } else if (acceptStr === FileAcceptType.ALL) {
        return '全部';
      } else {
        return acceptStr;
      }
    });
    return `接受${result.join('、')}类型的文件`;
  }
}

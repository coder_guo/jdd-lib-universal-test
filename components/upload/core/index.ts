import FileUpload, { FileStatus, FileAcceptType } from './core';
import type { WrappedFile, Config } from './core';

import {
  generateUUID,
  getDate,
  getFileNameFromUrl,
  getFileSuffixFromUrl,
} from './utils';

import Uploader from './uploader';

export {
  FileStatus,
  FileAcceptType,
  Uploader,
  generateUUID,
  getDate,
  getFileNameFromUrl,
  getFileSuffixFromUrl,
};
export type { FileUpload, WrappedFile, Config };
export default FileUpload;

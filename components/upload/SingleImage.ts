/**
 * 渲染页面，共有两种
 * ```
 * 1. 单图片模式，选择后即预览
 * 2. 附件模式，选择后依次展示，点击后预览
 *```
 */
import './index.css';
import FileUpload, { FileAcceptType, FileStatus, WrappedFile, Config, generateUUID, getFileNameFromUrl } from './core';

/** 单图片模式配置项 */
interface SingleImageUIConfig extends Partial<Config> {
  rootElement: HTMLElement;
}

/** 通用文件上传 - 单图片模式 */
export class SingleImage {
  // 上传实例
  private uploadInstance: FileUpload;
  // 上传时的遮罩
  private uploadMaskElement: HTMLElement;
  // 触发文件选择的元素
  private selectElement: HTMLElement;
  // 图片元素
  private imageElement: HTMLElement;
  // 底部文件名
  private fileNameElement: HTMLElement;
  // 上传失败布局
  private failElement: HTMLElement;
  // 上传成功布局
  private successElement: HTMLElement;
  // 只读
  private readonly: boolean = false;
  // 当前变化的文件
  private changedFile: WrappedFile;
  // 文件变化通知函数，如果是删除了文件，则传入null
  private _onNotify: (file: WrappedFile | null) => void;
  // dom id 前缀，避免多处使用时冲突
  private _idPrefix: string;
  constructor(options: SingleImageUIConfig) {
    const _this = this;
    this.readonly = options.readonly ?? false;
    this._idPrefix = generateUUID();
    this._onNotify = options.onNotify;
    this.uploadInstance = new FileUpload({
      readonly: options.readonly,
      maxCount: 1,
      maxExecCount: 1,
      accept: FileAcceptType.IMAGE,
      OSS_PREFIX: options.OSS_PREFIX || 'https://statics.wellyspring.com',
      getFileUploadURL: options.getFileUploadURL,
      onNotify(file) {
        _this.changedFile = file;
        _this.renderImageByFile(file);
        options.onNotify?.(file);
      },
    });
    this.render(options.rootElement);
  }
  /** 使用特定的值渲染，用于默认值处理 */
  renderDefaultFile(defaultValue: string) {
    if (defaultValue && defaultValue.length > 0) {
      const defaultFile: WrappedFile = {
        thumbURL: defaultValue,
        name: getFileNameFromUrl(defaultValue),
        status: FileStatus.SUCCESS,
        originFileObj: null,
        uuid: generateUUID(),
      };
      // 用于预览文件
      this.changedFile = defaultFile;
      this.renderImageByFile(defaultFile);
      this._onNotify?.(defaultFile);
    }
  }
  /**
   * 文件变化了，根据文件状态更新渲染
   * @param file 变化的文件
   */
  renderImageByFile(file: WrappedFile) {
    this.fileNameElement.innerText = file.name;
    // 支持鼠标移入展示全部文件名
    this.fileNameElement.setAttribute('title', file.name);
    // 图片预览
    this.imageElement.setAttribute('src', file.thumbURL);
    if (file.status === FileStatus.UPLOADING) {
      // uploading
      this._handleFileUploading();
    } else if (file.status === FileStatus.FAILED) {
      // upload fail
      this._handleFileUploadFail();
    } else {
      // upload success
      this._handleFileUploadSuccess();
    }
  }
  /** 初始状态，可以点击选择文件 */
  private _resetFileUpload() {
    this.selectElement.classList.remove('hide');
    this.failElement.classList.add('hide');
    this.imageElement.classList.add('hide');
    this.uploadMaskElement.classList.add('hide');
    this.successElement.classList.add('hide');
    this.fileNameElement.innerText = '文件名';
  }
  /** 上传成功，展示图片，同时鼠标移入可以展示浮层，进一步操作预览、删除 */
  private _handleFileUploadSuccess() {
    this.selectElement.classList.add('hide');
    this.failElement.classList.add('hide');
    this.successElement.classList.add('hide');
    this.imageElement.classList.remove('hide');
    this.uploadMaskElement.classList.add('hide');
  }
  /** 上传失败，提示上传失败，此时点击可以直接再次选择文件 */
  private _handleFileUploadFail() {
    this.selectElement.classList.add('hide');
    this.imageElement.classList.add('hide');
    this.failElement.classList.remove('hide');
    this.uploadMaskElement.classList.add('hide');
    this.successElement.classList.add('hide');
  }
  /** 文件正在上传中，展示浮层，展示loading效果 */
  private _handleFileUploading() {
    this.selectElement.classList.add('hide');
    this.imageElement.classList.remove('hide');
    this.failElement.classList.add('hide');
    this.uploadMaskElement.classList.remove('hide');
    this.successElement.classList.add('hide');
  }
  /** 渲染html结构 */
  render(rootElement: HTMLElement) {
    const innerHTML = `
    <div id="${this._idPrefix}-container" class="jdd-file-upload-container">
      <div id="${this._idPrefix}-preview" class="jdd-file-upload-preview">
        <div id="${this._idPrefix}-select" class="jdd-file-upload-select ${this.readonly ? 'hide' : ''}">+</div>
        <img id="${this._idPrefix}-image" src="" class="jdd-file-upload-image hide"></img>
        <div id="${this._idPrefix}-success" class="jdd-file-upload-success hide">
            <div id="${this._idPrefix}-preview-icon" class="jdd-file-preview-icon jdd-icon-eye"></div>
            <div id="${this._idPrefix}-delete-icon" class="jdd-file-delete-icon jdd-icon-trash ${
      this.readonly ? 'hide' : ''
    }"></div>
        </div>
        <div id="${this._idPrefix}-fail" class="jdd-file-upload-fail hide">
          <div class="jdd-file-upload-fail-icon">!</div>
          <p class="jdd-file-upload-fail-text">上传失败</p>
        </div>
        <div id="${
          this._idPrefix
        }-loading-mask" class="jdd-file-upload-mask hide"><span class="icon-shape-half-circle anim-loading"></span></div>
      </div>
      <p id="${this._idPrefix}-name" class="jdd-file-upload-name">文件名</p>
    </div>
  `;
    rootElement.innerHTML = innerHTML;
    this.imageElement = document.getElementById(`${this._idPrefix}-image`);
    this.selectElement = document.getElementById(`${this._idPrefix}-select`);
    this.fileNameElement = document.getElementById(`${this._idPrefix}-name`);
    this.uploadMaskElement = document.getElementById(`${this._idPrefix}-loading-mask`);
    this.failElement = document.getElementById(`${this._idPrefix}-fail`);
    this.successElement = document.getElementById(`${this._idPrefix}-success`);
    this.selectElement.addEventListener('click', () => {
      this.uploadInstance.choose();
    });
    this.failElement.addEventListener('click', () => {
      this.uploadInstance.choose();
    });
    this.imageElement.addEventListener('mouseenter', () => {
      this.successElement.classList.remove('hide');
    });
    document.getElementById(`${this._idPrefix}-preview`).addEventListener('mouseleave', () => {
      this.successElement.classList.add('hide');
    });
    // 预览
    document.getElementById(`${this._idPrefix}-preview-icon`).addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();
      window.open(this.changedFile.thumbURL);
    });
    // 删除
    document.getElementById(`${this._idPrefix}-delete-icon`).addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();
      this.changedFile = null;
      this._resetFileUpload();
      this._onNotify?.(null);
    });
  }
}

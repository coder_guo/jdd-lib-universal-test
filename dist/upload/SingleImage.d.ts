/**
 * 渲染页面，共有两种
 * ```
 * 1. 单图片模式，选择后即预览
 * 2. 附件模式，选择后依次展示，点击后预览
 *```
 */
import './index.css';
import { WrappedFile, Config } from './core';
/** 单图片模式配置项 */
interface SingleImageUIConfig extends Partial<Config> {
    rootElement: HTMLElement;
}
/** 通用文件上传 - 单图片模式 */
export declare class SingleImage {
    private uploadInstance;
    private uploadMaskElement;
    private selectElement;
    private imageElement;
    private fileNameElement;
    private failElement;
    private successElement;
    private readonly;
    private changedFile;
    private _onNotify;
    private _idPrefix;
    constructor(options: SingleImageUIConfig);
    /** 使用特定的值渲染，用于默认值处理 */
    renderDefaultFile(defaultValue: string): void;
    /**
     * 文件变化了，根据文件状态更新渲染
     * @param file 变化的文件
     */
    renderImageByFile(file: WrappedFile): void;
    /** 初始状态，可以点击选择文件 */
    private _resetFileUpload;
    /** 上传成功，展示图片，同时鼠标移入可以展示浮层，进一步操作预览、删除 */
    private _handleFileUploadSuccess;
    /** 上传失败，提示上传失败，此时点击可以直接再次选择文件 */
    private _handleFileUploadFail;
    /** 文件正在上传中，展示浮层，展示loading效果 */
    private _handleFileUploading;
    /** 渲染html结构 */
    render(rootElement: HTMLElement): void;
}
export {};

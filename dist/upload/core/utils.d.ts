/** 判断值不为null或者undefined */
export declare function valueNotNullAndUndefined(val: any): boolean;
/** 简单的对当前日期做格式化处理：YYYY-MM-DD */
export declare function getDate(): string;
/** 按照固定格式生成一个UUID，用于上传文件时添加到文件名前部以避免文件名冲突 */
export declare function generateUUID(): string;
/**
 * 从地址中获取文件名和后缀
 * @param url 地址
 */
export declare function getFileNameFromUrl(url: string): string;
/**
 * 从地址或者文件名中获取后缀
 * @param url 文件地址
 * @returns 后缀
 */
export declare function getFileSuffixFromUrl(url: string): string;
/**
 * 判断给定的文件是否符合accept的要求；判断文件类型和文件后缀
 * @param file
 * @param accept
 */
export declare function isFileMatchAccept(file: File, accept: string): boolean;

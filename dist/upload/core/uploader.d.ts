/**
 * 将文件上传到oss
 * 文件会先放到队列中，然后通知上传，上传过程中会更新文件状态并通知到外部
 */
import type { WrappedFile } from './core';
/** config */
interface Config {
    /**
     * 文件上传后访问前缀地址
     */
    OSS_PREFIX: string;
    /**
     * 最大同时执行任务数
     * @default 3
     */
    maxExecCount: number;
    /**
     * 上传文件时调用方法获取文件完整存放地址
     * @param File file 待上传的文件
     */
    getFileUploadURL: (file: WrappedFile) => Promise<string>;
    /**
     * 本地文件变化时回调函数
     * @param WrappedFile file 变化的文件
     */
    onNotify: (file: WrappedFile) => void;
}
/**
 * 外部传入的配置项，除maxExecCount外全部需要必填
 */
export type UploaderConfig = Required<Omit<Config, 'maxExecCount'>> & Partial<Pick<Config, 'maxExecCount'>>;
/**
 * 最多同时执行3个文件上传
 * 每次有新的文件添加到队列中时要触发上传动作
 * 队列中所有文件都上传完毕后停止上传
 * 上传过程中要一直更新文件状态
 */
declare class Uploader {
    /** 待上传的文件列表 */
    private queue;
    /** 当前正在执行的任务数 */
    private executingCount;
    /** 当前是否有任务正在执行 */
    private isExecuting;
    /** 配置 */
    private config;
    constructor(config: UploaderConfig);
    /**
     * 将选中的文件添加到上传队列，同时触发上传任务执行
     * @param files
     */
    addFileToQueue(files: WrappedFile[]): void;
    /** 执行上传任务，会按照配置同时处理N个任务，直到文件列表全部上传完成 */
    exec(): void;
    /**
     * 调用接口获取oss的上传地址，会返回sign等内容
     * @param file
     * @return Promise<string>
     */
    private _getFileUploadUrl;
    /**
     * 本地开发环境直接请求PUT到OSS，会提示跨域（oss有配置过限制规则），为了解决跨域同时保证oss访问的安全性，判断如果是本地开发环境，直接走代理
     * @param url 上传地址
     * @returns
     */
    private _parseLocalDevUrl;
    /** 图片上传到oss方法 */
    private _uploadFileToOSS;
}
export default Uploader;

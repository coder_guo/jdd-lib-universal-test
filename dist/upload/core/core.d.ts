import type { UploaderConfig } from './uploader';
/**
 * 文件选择之后的状态可以根据状态展示不同的UI
 * ```
 * DEFAULT: 默认状态，选择之后即该状态
 * UPLOADING: 上传中
 * SUCCESS: 上传成功
 * FAILED: 上传失败
 * ```
 */
export declare enum FileStatus {
    DEFAULT = "default",
    UPLOADING = "uploading",
    SUCCESS = "success",
    FAILED = "failed"
}
/**
 * 可选的文件类型
 * ```ts
 * ALL - 所有文件
 * IMAGE - 仅图片文件
 * AUDIO - 仅音频文件
 * VIDEO - 仅视频文件
 * OFFICE - 仅Office文件
 * ```
 */
export declare enum FileAcceptType {
    ALL = "*",
    IMAGE = "image/*",
    AUDIO = "audio/*",
    VIDEO = "video/*",
    PDF = ".pdf",
    OFFICE = ".doc,.docx,.xls,.xlsx,.ppt,.pptx"
}
/**
 * 文件上传的基础配置项
 */
interface CoreConfig {
    /** 只读模式，不能操作，只用于展示;default:false */
    readonly?: boolean;
    /**
     * 文件选择时默认展示的文件类型
     * @default ALL
     */
    accept?: FileAcceptType;
    /**
     * 最多允许选择几个文件
     * @default 1
     */
    maxCount?: number;
    /**
     * 每个文件的大小限制
     * @default 5MB
     */
    maxSize?: number;
}
export type Config = CoreConfig & UploaderConfig;
/** 本地选择的文件，二次包装 */
export interface WrappedFile extends Partial<File> {
    /** 文件唯一的id，自动生成 */
    uuid: string;
    /** 文件的预览地址，如果是本地选择，预览地址使用URL.createObjectURL生成，如果是oss外部地址，直接是具体的访问链接 */
    thumbURL: string;
    /** 状态 */
    status: FileStatus;
    /** 本地选择时，存在originFileObj */
    originFileObj?: File | null;
}
/**
 * 文件上传核心功能
 * ```code
 * 1. 选择文件
 * 2. 配置文件类型、大小、过滤规则、如何获取上传链接、生成唯一uuid等
 * 3. 上传到oss，并将实际的访问地址返回
 * 4. 文件状态变化时通知外部调用方
 * ```
 */
declare class FileUploadCore {
    /** 默认配置项 */
    private config;
    private uploader;
    /**
     * 初始化方法
     * @param {Config} options 配置项
     */
    constructor(options: Config);
    /**
     * 触发文件选择
     * @return
     * ```ts
     * Promise(WrappedFile[])
     * ```
     */
    choose(): Promise<File[]>;
    /**
     * 更新当前已选的文件列表，用于外部删除了文件之后同步到组件内部
     * @access public
     * @param {WrappedFile[]} files 更新之后的完整文件列表
     */
    /** 将选择的文件和已选的文件合并 */
    private _mergeSelectedFiles;
    /** 将本地选择的文件转为组件需要的格式 */
    private _convertFileToUploadFile;
    /**
     * 根据配置生成对应的input元素，触发文件选择
     * @access private
     * @returns 生成的input元素
     */
    private _generateInputElement;
}
export default FileUploadCore;

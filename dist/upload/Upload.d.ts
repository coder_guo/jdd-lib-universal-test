import { FC } from 'react';
import { FileAcceptType, WrappedFile } from './core';
interface FileUploadProps {
    readonly?: boolean;
    accept?: FileAcceptType;
    value?: string;
    onChange?: (v: WrappedFile | WrappedFile[]) => void;
    maxCount?: number;
    /** 上传文件的路径前缀，如: /universal/upload，不需要结尾的/ */
    directory: string;
    type: 'single' | 'attachment';
}
declare const FileUpload: FC<FileUploadProps>;
export default FileUpload;

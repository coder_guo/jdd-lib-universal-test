/**
 * 渲染页面，共有两种
 * ```
 * 1. 单图片模式，选择后即预览
 * 2. 附件模式，选择后依次展示，点击后预览
 *```
 */
import './index.css';
import { WrappedFile, Config } from './core';
/** 单图片模式配置项 */
interface AttachmentUIConfig extends Partial<Omit<Config, 'onNotify'>> {
    rootElement: HTMLElement;
    onNotify?: (files: WrappedFile[] | null) => void;
}
/** 通用文件上传 - 附件模式 */
export declare class Attachment {
    private uploadInstance;
    private selectElement;
    private filesElement;
    private allFiles;
    private maxCount;
    private _onNotify;
    private readonly;
    private _idPrefix;
    private accept;
    constructor(options: AttachmentUIConfig);
    /** 根据文件状态生成文件item */
    renderFileItem(file: WrappedFile): string;
    renderAllFiles(): void;
    /** 使用特定的值渲染，用于默认值处理 */
    renderDefaultFile(defaultValue: string[]): void;
    /** 渲染html结构 */
    render(rootElement: HTMLElement): void;
    /** 将Accept的文件类型默认展示，方便用户选择时判断
     *
     * accept=.pdf,image/*,*
     */
    formatFileAccept(): string;
}
export {};

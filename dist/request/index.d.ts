export declare function setReLoginHandle(fn: () => void): void;
interface FetcherInitialOptions extends RequestInit {
    params?: Record<string, any>;
    onResponse?: (v: any) => void;
}
/** 默认的请求函数，对原生的fetch添加了处理 */
declare const fetcher: <T = null>(url: string, init?: FetcherInitialOptions) => Promise<T>;
export default fetcher;

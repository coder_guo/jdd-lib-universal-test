import { S as l, A as r } from "./Attachment-7feb51e5.js";
import { F as _, a as I } from "./Attachment-7feb51e5.js";
import { openBlock as c, createElementBlock as u } from "vue";
const d = {
  props: ["modelValue", "directory", "type", "maxCount", "readonly", "accept"],
  emits: ["update:modelValue", "change"],
  data() {
    return {
      uiInstance: null
    };
  },
  watch: {
    modelValue: {
      handler(e) {
        var n, s, o, i;
        typeof e == "string" && (this.type === "single" ? (s = (n = this.uiInstance) == null ? void 0 : n.renderDefaultFile) == null || s.call(n, e) : (i = (o = this.uiInstance) == null ? void 0 : o.renderDefaultFile) == null || i.call(o, e.split(",")));
      },
      deep: !0
    }
  },
  methods: {
    getFileSuf(e) {
      const n = e.name.split(".");
      return `.${n[n.length - 1]}`;
    }
  },
  mounted() {
    var s, o, i, a;
    const e = this, n = this.$refs.uploadContainer;
    this.type === "single" ? this.uiInstance = new l({
      readonly: e.readonly,
      rootElement: n,
      getFileUploadURL(t) {
        return Promise.resolve(`${e.directory}/${t.uuid}${e.getFileSuf(t)}`);
      },
      onNotify(t) {
        console.log("notify:", t), e.$emit("update:modelValue", t), e.$emit("change", t);
      }
    }) : e.type === "attachment" ? this.uiInstance = new r({
      readonly: e.readonly,
      accept: e.accept,
      rootElement: n,
      maxCount: e.maxCount,
      getFileUploadURL(t) {
        return console.log("file", t), Promise.resolve(`${e.directory}/${t.uuid}${e.getFileSuf(t)}`);
      },
      onNotify(t) {
        console.log("notify:", t), e.$emit("update:modelValue", t), e.$emit("change", t);
      }
    }) : console.warn("不支持该模式:%s,目前仅支持single/attachment", e.type), typeof this.modelValue == "string" && (this.type === "single" ? (o = (s = this.uiInstance).renderDefaultFile) == null || o.call(s, this.modelValue) : (a = (i = this.uiInstance).renderDefaultFile) == null || a.call(i, this.modelValue.split(",")));
  }
}, p = (e, n) => {
  const s = e.__vccOpts || e;
  for (const [o, i] of n)
    s[o] = i;
  return s;
}, m = { ref: "uploadContainer" };
function h(e, n, s, o, i, a) {
  return c(), u("div", m, null, 512);
}
const g = /* @__PURE__ */ p(d, [["render", h]]);
export {
  _ as FileAcceptType,
  I as FileStatus,
  g as FileUpload
};
